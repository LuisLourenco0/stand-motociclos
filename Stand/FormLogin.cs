﻿using System;
using System.Data;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Stand
{
    public partial class FormInicial : Form
    {
        /**
         * @brief Classe FormInicial que é uma sub-classe da classe Form
         */

        public FormInicial()
        {
            InitializeComponent();
        }

        static string server = "localhost"; /*!<String estática que armazena o nome do servidor neste caso está a ser utilizado o localhost */

        static string database = "database_motas"; /*!<String estática que armazena o nome da base de dados a utilizar.*/

        static string uid = "root"; /*!<String estática que armazena o nome de utilizadar da base de dados.*/

        static string password = ""; /*!<String estática que armazena a password de acesso á base de dados.*/

        static string connectionString = "SERVER=" + server + ";" + "DATABASE=" + database + ";" + "UID=" + uid + ";" + "PASSWORD=" + password + ";"; /*!<String de conexão á base de dados*/



        /** Evento Click, que ativado quando o botão buttonLogin é clicado pelo utilizador, abre a conexão com a base de dados
         * e envia um script sql que seleciona o nome de utilizador e password, e compara as informações que o utilizador introduziu
         * com as informações que chegam da base de dados, se forem iguai efetua o login e muda para o form (FormGerirMotas).
         * 
         * 
         * @param sender é um parâmetro que contém uma referência ao controle/objeto que ativou o evento.
         * @param e é um parâmetro que contém os dados do evento.
         */
        private void buttonLogin_Click(object sender, EventArgs e)
        {
            try
            {
                MySqlConnection connection = new MySqlConnection(connectionString);
                connection.Open();
                if (textBoxNome.Text != "" && textBoxPassword.Text != "")
                {
                    
                    string query = "Select * FROM users WHERE Nome_User = '" + textBoxNome.Text.Trim() + "' AND Password_user = '" + textBoxPassword.Text.Trim() + "'";
                    MySqlDataAdapter sda = new MySqlDataAdapter(query, connection);
                    DataTable dataTable = new DataTable();
                    sda.Fill(dataTable);

                    if (dataTable.Rows.Count == 1)
                    {
                        FormGerirMotas Form2 = new FormGerirMotas();
                        this.Hide();
                        connection.Close();
                        Form2.Show();
                    }
                    else
                    {
                        Console.WriteLine("Erro ao mudar de Form");
                    }
                }
                
            }
            catch (Exception e1)
            {
                Console.WriteLine("ERRO - - - " + e1);
            }

        }


        /** Evento Click, que é ativado quando o botão buttonRegister é clicado, é responsável por adicionar um novo utilizador á base de dados.
         * 
         * 
         * @param sender é um parâmetro que contém uma referência ao controle/objeto que ativou o evento.
         * @param e é um parâmetro que contém os dados do evento.
         */
        private void buttonRegister_Click(object sender, EventArgs e)
        {
            try
            {
                using (MySqlConnection connection = new MySqlConnection(connectionString))
                {
                    string query = "INSERT INTO users (Nome_User, Password_User) VALUES (@Nome_User, @Password_User)";
                    connection.Open();

                    using (MySqlCommand cmd = new MySqlCommand(query, connection))
                    {
                        cmd.Parameters.Add("@Nome_User", MySqlDbType.VarChar).Value = textBoxNome.Text.Trim();
                        cmd.Parameters.Add("@Password_User", MySqlDbType.VarChar).Value = textBoxPassword.Text.Trim();
                        cmd.ExecuteNonQuery();

                        var result = MessageBox.Show("Utilizador Registado com Sucesso\n", "Sucesso", MessageBoxButtons.OK);

                    }
                }
            }
            catch (Exception e1)
            {
                Console.WriteLine(e1.ToString());
            }

        }

        /** Evento Click, que é ativado quando o botão buttonSair é clicado, é responsável por fechar a aplicação.
         * 
         * 
         * @param sender é um parâmetro que contém uma referência ao controle/objeto que ativou o evento.
         * @param e é um parâmetro que contém os dados do evento.
         */
        private void buttonSair_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }


    }
}
