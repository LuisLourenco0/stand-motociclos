﻿using System;
using System.Data;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Stand
{
    public partial class FormGerirMotas : Form
    {
        /**
         * @brief Classe FormGerirMotas que é uma sub-classe da classe Form
         */
        public FormGerirMotas()
        {
            InitializeComponent();
        }
        

        static string server = "localhost"; /*!<String estática que armazena o nome do servidor neste caso está a ser utilizado o localhost */

        static string database = "database_motas"; /*!<String estática que armazena o nome da base de dados a utilizar.*/

        static string uid = "root"; /*!<String estática que armazena o nome de utilizadar da base de dados.*/

        static string password = ""; /*!<String estática que armazena a password de acesso á base de dados.*/

        static string connectionString = "SERVER=" + server + ";" + "DATABASE=" + database + ";" + "UID=" + uid + ";" + "PASSWORD=" + password + ";"; /*!<String de conexão á base de dados*/



        /** Evento Load que carrega o Form, quando carrega define a list View, para vista detalhada e mostra linhas
         * da grelha e define a seleção em linha completa.
         * 
         * @param sender é um parâmetro que contém uma referência ao controle / objeto que ativou o evento.
         * @param e é um parâmetro que contém os dados do evento.
         */
        private void FormGerirMotas_Load(object sender, EventArgs e)
        {
            listView1.View = View.Details;
            listView1.GridLines = true;
            listView1.FullRowSelect = true;
        }

        /** Evento Click, que ativado quando o botão buttonUpdate é clicado pelo utilizador, abre a conexão com a base de dados
         * e envia um script sql que seleciona todos os dados da base dados e adiciona os dados á ListView.
         * 
         * 
         * @param sender é um parâmetro que contém uma referência ao controle/objeto que ativou o evento.
         * @param e é um parâmetro que contém os dados do evento.
         */
        private void buttonUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                listView1.Items.Clear();
                listView1.Columns.Clear(); 

                MySqlConnection connection = new MySqlConnection(connectionString);
                MySqlDataAdapter adapter = new MySqlDataAdapter("Select * FROM motas", connection);
                DataTable dataTable = new DataTable();
                adapter.Fill(dataTable);

                for (int i = 0; i < dataTable.Rows.Count; i++)
                {
                    DataRow row = dataTable.Rows[i];
                    ListViewItem item1 = new ListViewItem("item1", 0);

                    ListViewItem listItem = new ListViewItem(row["ID_Motas"].ToString());
                    listItem.SubItems.Add(row["Marca_Motas"].ToString());
                    listItem.SubItems.Add(row["Modelo_Motas"].ToString());
                    listItem.SubItems.Add(row["Cor_Motas"].ToString());
                    listItem.SubItems.Add(row["Comprimento_Motas"].ToString());
                    listItem.SubItems.Add(row["Matricula_Motas"].ToString());
                    listItem.SubItems.Add(row["Cilindrada_Motas"].ToString());


                    listView1.Items.AddRange(new ListViewItem[] { listItem });
                }
                listView1.Columns.Add("ID", 35);
                listView1.Columns.Add("Marca", 60);
                listView1.Columns.Add("Modelo", 75);
                listView1.Columns.Add("Cor", 60);
                listView1.Columns.Add("Comprimento", 80);
                listView1.Columns.Add("Matricula", 60);
                listView1.Columns.Add("Cilindrada", 60);
            }
            catch (Exception e1)
            {
                Console.WriteLine(e1.ToString());
            }
        }


        /** Evento Click, que é ativado quando o botão buttonAdd é clicado pelo utilizador, é responsável
         * por abrir um novo Form, neste caso o (formAddNovaMota) e fecha o form atual (FormGerirMotas).
         * 
         * @param sender é um parâmetro que contém uma referência ao controle/objeto que ativou o evento.
         * @param e é um parâmetro que contém os dados do evento.
         */
        private void buttonAdd_Click(object sender, EventArgs e)
        {
            FormAddNovaMota formAddNovaMota = new FormAddNovaMota();
            this.Close();
            formAddNovaMota.Show();
        }
    }
}
