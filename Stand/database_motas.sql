-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 21, 2021 at 03:35 PM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.2.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `database_motas`
--

-- --------------------------------------------------------

--
-- Table structure for table `motas`
--

CREATE TABLE `motas` (
  `ID_Motas` int(11) NOT NULL,
  `Marca_Motas` varchar(10) DEFAULT NULL,
  `Modelo_Motas` varchar(10) DEFAULT NULL,
  `Cor_Motas` varchar(10) DEFAULT NULL,
  `Comprimento_motas` varchar(10) DEFAULT NULL,
  `Matricula_Motas` varchar(10) DEFAULT NULL,
  `Cilindrada_Motas` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `motas`
--

INSERT INTO `motas` (`ID_Motas`, `Marca_Motas`, `Modelo_Motas`, `Cor_Motas`, `Comprimento_motas`, `Matricula_Motas`, `Cilindrada_Motas`) VALUES
(1, 'FAMEL', 'Zundapp XF', 'Vermelha', '120', 'AS-32-RF', 50),
(2, 'Casal', 'V5', 'Vermelha', '150', 'FD-45-SA', 50);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `ID_User` int(11) NOT NULL,
  `Nome_User` varchar(15) DEFAULT NULL,
  `Password_User` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`ID_User`, `Nome_User`, `Password_User`) VALUES
(1, 'Luis', '1234'),
(2, 'bagarrao', '12345');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `motas`
--
ALTER TABLE `motas`
  ADD PRIMARY KEY (`ID_Motas`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`ID_User`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `motas`
--
ALTER TABLE `motas`
  MODIFY `ID_Motas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `ID_User` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
