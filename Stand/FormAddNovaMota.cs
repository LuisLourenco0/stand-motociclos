﻿using System;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Stand
{
    public partial class FormAddNovaMota : Form
    {
        /**
         * @brief Classe FormAddNovaMota que é uma sub-classe da classe Form
         */

        public FormAddNovaMota()
        {
            InitializeComponent();
        }

        static string server = "localhost"; /*!<String estática que armazena o nome do servidor neste caso está a ser utilizado o localhost */

        static string database = "database_motas"; /*!<String estática que armazena o nome da base de dados a utilizar.*/

        static string uid = "root"; /*!<String estática que armazena o nome de utilizadar da base de dados.*/

        static string password = ""; /*!<String estática que armazena a password de acesso á base de dados.*/

        static string connectionString = "SERVER=" + server + ";" + "DATABASE=" + database + ";" + "UID=" + uid + ";" + "PASSWORD=" + password + ";"; /*!<String de conexão á base de dados*/


        /** Evento Click, que ativado quando o botão buttonSave é clicado pelo utilizador, abre a conexão com da base de dados
         * e é responsável por adicionar uma mota nova á base de dados.
         * 
         * 
         * 
         * @param sender é um parâmetro que contém uma referência ao controle/objeto que ativou o evento.
         * @param e é um parâmetro que contém os dados do evento.
         */
        private void buttonSave_Click(object sender, EventArgs e)
        {
            try
            {
                using (MySqlConnection connection = new MySqlConnection(connectionString))
                {
                    string query = "INSERT INTO motas (Marca_Motas, Modelo_Motas, Cor_Motas, Comprimento_Motas, Matricula_Motas, Cilindrada_Motas) VALUES (@Marca_Motas, @Modelo_Motas, @Cor_Motas, @Comprimento_Motas, @Matricula_Motas, @Cilindrada_Motas)";
                    connection.Open();
                    using (MySqlCommand cmd = new MySqlCommand(query, connection))
                    {
                        cmd.Parameters.Add("@Marca_Motas", MySqlDbType.VarChar).Value = textBoxMarca.Text.Trim();
                        cmd.Parameters.Add("@Modelo_Motas", MySqlDbType.VarChar).Value = textBoxModelo.Text.Trim();
                        cmd.Parameters.Add("@Cor_Motas", MySqlDbType.VarChar).Value = textBoxCor.Text.Trim();
                        cmd.Parameters.Add("@Comprimento_Motas", MySqlDbType.Int32).Value = textBoxComprimento.Text.Trim();
                        cmd.Parameters.Add("@Matricula_Motas", MySqlDbType.VarChar).Value = textBoxMatricula.Text.Trim();
                        cmd.Parameters.Add("@Cilindrada_Motas", MySqlDbType.Int32).Value = textBoxCilindrada.Text.Trim();
                        cmd.ExecuteNonQuery();

                        var result = MessageBox.Show("Motorizada Guardada com sucesso. \n Pretende adicionar mais?", "Sucesso", MessageBoxButtons.YesNo);

                        if(result == DialogResult.Yes)
                        {
                            textBoxMarca.Clear();
                            textBoxModelo.Clear();
                            textBoxCor.Clear();
                            textBoxComprimento.Clear();
                            textBoxMatricula.Clear();
                            textBoxCilindrada.Clear();
                        }
                        else
                        {
                            FormGerirMotas form2 = new FormGerirMotas();
                            this.Close();
                            connection.Close();
                            form2.Show();
                        }
                        connection.Close();
                    }
                }
            }
            catch (Exception e1)
            {
                Console.WriteLine("ERRO - - - " + e1.ToString());
            }
        }
    }
}
